from heapq import heapify, heappop, heappush
from operator import itemgetter
from random import randrange
from simulator import ARRIVAL, COMPLETE, INTERNAL

inf = float('inf')

def random_queue(est, assignments, estimations, work_done):
    return randrange(len(assignments))

def shortest_queue(est, assignments, estimations, work_done):
    return min((len(a), q) for q, a in enumerate(assignments))[1]

def least_loaded_total(est, assignments, estimations, work_done):
    return min((sum(estimations[j] for j in a), q)
               for q, a in enumerate(assignments))[1]    

def least_loaded_updated(est, assignments, estimations, work_done):
    return min((sum(estimations[j] - work_done[j] for j in a), q)
               for q, a in enumerate(assignments))[1]

def least_loaded_non_negative(est, assignments, estimations, work_done):
    return min((sum(max(0, estimations[j] - work_done[j]) for j in a), q)
               for q, a in enumerate(assignments))[1]

def selfish_ll_total(est, assignments, estimations, work_done):
    allest = ((q, (estimations[jobid] for jobid in a))
              for q, a in enumerate(assignments))
    return min((sum(estj for estj in es if estj <= est), q)
               for q, es in allest)[1]

event_typenames = {ARRIVAL: 'ARRIVAL', COMPLETE: 'COMPLETE', INTERNAL: 'INTERNAL'}

def supermarket(jobs, schedulers, choose_queue):

    events = [(t, ARRIVAL, (jobid, size, est))
              for jobid, t, size, est in jobs]
    
    heapify(events)
    last_t = events[0][0]

    schedules = [{} for _ in schedulers]  # mapping jobid to resource ratio
    overall_schedule = {}
    sizes = {}  # mapping jobid to size
    estimations = {}  # jobid to estimation
    work_done = {}  # jobid to work done
    assignments = [set() for _ in schedulers]  # queue to jobid set
    # [(time, [COMPLETE|INTERNAL|None], event_data)]
    next_events = [(inf, None, None) for _ in schedulers]  

    while events:  # main loop
        t, event_type, event_data = heappop(events)
        delta = t - last_t
        assert delta >= 0, (t, last_t, event_type, event_data)

        # update work done
        for jobid, resources in overall_schedule.items():
            work_done[jobid] += resources * delta

        # process events (and call relevant schedulers)
        if event_type == ARRIVAL:
            jobid, size, est = event_data
            sizes[jobid] = size
            estimations[jobid] = est
            work_done[jobid] = 0
            queue = choose_queue(est, assignments, estimations, work_done)
            assignments[queue].add(jobid)
            schedulers[queue].enqueue(t, jobid, est)
        elif event_type == COMPLETE:
            queue, jobid = event_data
            yield t, jobid
            del estimations[jobid]
            del work_done[jobid]
            del sizes[jobid]
            schedulers[queue].dequeue(t, jobid)
            assignments[queue].remove(jobid)
        elif event_type == INTERNAL:  # internal event
            queue = event_data
        else:
            raise ValueError('Unknown event type', event_type)
        # all branches have now set queue
        
        # update the schedule
        scheduled_before = schedules[queue].keys()
        scheduler = schedulers[queue]
        schedule = schedules[queue] = scheduler.schedule(t)
        overall_schedule.update(schedule)
        for j in scheduled_before - schedule:
            del overall_schedule[j]
        # next job completion for this queue
        completions = [((sizes[jobid] - work_done[jobid]) / resources, jobid)
                       for jobid, resources in schedule.items()]
        try:
            next_delta, next_jobid = min(completions)
            assert next_delta >= -1e-10, (schedule, next_delta, next_jobid, work_done[next_jobid], sizes[next_jobid])
        except ValueError:
            next_delta, next_type, next_data = inf, None, None
        else:
            next_type, next_data = COMPLETE, (queue, next_jobid)
        # this queue's next internal event
        ni = scheduler.next_internal_event()
        if ni is not None and ni < next_delta:
            next_type, next_delta, next_data = INTERNAL, ni, queue
        next_t = t + next_delta
        next_events[queue] = (t + next_delta, next_type, next_data)

        cand_event = cand_t, *_ = min(next_events)
        if cand_t < inf and (not events or cand_t < events[0][0]):
            heappush(events, cand_event)

        last_t = t
