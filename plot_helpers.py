from itertools import cycle
import math

from cycler import cycler
from matplotlib import pyplot as plt

def config_paper(font_size=22):
    SMALL_SIZE = 14
    MEDIUM_SIZE = 16
    BIGGER_SIZE = 18

    plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
    plt.rc('font', family='Palatino')
#    plt.rc('font', family='serif', serif=['Palatino'])
    plt.rc('text', usetex=True)
    #if font_size:
        # 22 looks good for double-column figures
    #    plt.rcParams.update({'font.size': font_size})

plt.style.use('seaborn-paper')

line_styles = "- -- -. :".split()
        
cycler1 = plt.rcParams['axes.prop_cycle']
cycler2 = cycler('linestyle', line_styles)
a, b = len(cycler1), len(cycler2)
lcm = a * b // math.gcd(a,b)
cycler1 = cycler1 * (lcm // a)
cycler2 = cycler2 * (lcm // b)
plt.rc('axes', prop_cycle=(cycler1 + cycler2))
plt.rc('lines', linewidth=3)
plt.rc('axes.spines', left=False, right=False, top=False, bottom=False)
        
line_styles = "- -- -. :".split()
def cycle_styles(marker=''):
    return cycle(s + marker for s in line_styles)
